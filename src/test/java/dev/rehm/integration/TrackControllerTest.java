package dev.rehm.integration;


import dev.rehm.models.Track;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.junit4.SpringRunner;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;



@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
public class TrackControllerTest {


    @Test
    public void testGetOneShouldReturnCorrectItem(){
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "auth-token");
        HttpEntity<Track> request = new HttpEntity<Track>(headers);
        ResponseEntity<Track> responseEntity = new TestRestTemplate().exchange("http://localhost:8082/tracks/2",
                HttpMethod.GET,
                request,
                Track.class);

        Track actual = responseEntity.getBody();
        Track expected = new Track(2,"Rocky Raccoon","The Beatles",  1968);
        assertEquals(expected,actual);
    }

    @Test
    public void testOneUnauthorizedReturnNull(){
        Track actual = new TestRestTemplate().getForObject("http://localhost:8082/tracks/2", Track.class);
        assertNull(actual);
    }

}
