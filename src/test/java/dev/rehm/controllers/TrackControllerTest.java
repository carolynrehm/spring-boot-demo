package dev.rehm.controllers;

import dev.rehm.aspects.AuthAspect;
import dev.rehm.models.Track;
import dev.rehm.services.TrackService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.ImportAutoConfiguration;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.hamcrest.Matchers.hasItems;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

//@SpringBootTest
@WebMvcTest
public class TrackControllerTest {

    private MockMvc mockMvc;

    @MockBean
    TrackService trackService;

    @Autowired
    TrackController trackController;

    @BeforeEach
    public void setup(){
        this.mockMvc = MockMvcBuilders.standaloneSetup(trackController)
                .build();
    }

    @Test
    public void shouldReturnCorrectItems() throws Exception {
        List<Track> tracks = new ArrayList<>();
        tracks.add(new Track(1, "song 1", "artist 1", 2001));
        tracks.add(new Track(2, "song 2", "artist 2", 2002));
        tracks.add(new Track(3, "song 3", "artist 3", 2003));

        when(trackService.getAllTracks()).thenReturn(tracks);

        this.mockMvc.perform(
                get("/tracks").header("Authorization", "admin-token"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[*].name")
                        .value(hasItems("song 1", "song 2", "song 3")));
    }

    @Test
    public void shouldReturnNewItem() throws Exception {
        this.mockMvc.perform(post("/tracks")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{ \"name\":\"blah\", \"artist\":\"blah\", \"releaseYear\":1993 }"))
                    .andExpect(status().isCreated())
                    .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                    .andExpect(jsonPath("$.name").value("blah"));
    }




}
