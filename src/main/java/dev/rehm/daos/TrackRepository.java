package dev.rehm.daos;

import dev.rehm.models.Track;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface TrackRepository extends JpaRepository<Track,Integer> {

    List<Track> findByReleaseYear(int year);
    List<Track> findByArtist(String artist);
    List<Track> findByArtistAndReleaseYear(String artist, int year);

}
