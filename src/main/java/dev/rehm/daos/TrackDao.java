package dev.rehm.daos;

import dev.rehm.models.Track;

import java.util.List;

public interface TrackDao {

    public List<Track> getAllTracks();
    public Track createNewTrack(Track t);

}
