package dev.rehm.daos;

import dev.rehm.models.Track;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class TrackDaoImpl implements TrackDao {

    private List<Track> tracks = new ArrayList<>();

    public TrackDaoImpl(){
        tracks.add(new Track(1, "Bad Moon Rising","Creedence Clearwater Revival",1969));
        tracks.add(new Track(2, "Rocky Raccoon","The Beatles",1968));
        tracks.add(new Track(3, "Something","The Beatles",1969));
        tracks.add(new Track(4, "Michelle","The Beatles",1965));
    }

    @Override
    public List<Track> getAllTracks() {
        return new ArrayList<>(tracks);
    }

    @Override
    public Track createNewTrack(Track t) {
        tracks.add(t);
        return t;
    }
}
