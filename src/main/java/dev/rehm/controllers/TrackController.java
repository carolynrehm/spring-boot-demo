package dev.rehm.controllers;

import dev.rehm.models.Track;
import dev.rehm.services.TrackService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

//@Controller
@RestController
@CrossOrigin
@RequestMapping("/tracks")
public class TrackController {

    @Autowired
    private TrackService trackService;

    /*
        GET     /tracks
        GET     /tracks/{id}
        POST    /tracks
        PUT     /tracks/{id}
        DELETE  /tracks/{id}
     */

    @GetMapping(produces = "application/json")
//    @ResponseBody
    public ResponseEntity<List<Track>> returnAllTracks(@RequestParam(value = "year",required = false)Integer year,
                                        @RequestParam(value =
            "artist", required = false)String artist){
        if(year!=null) {
            if (artist != null) {
                return new ResponseEntity<>(trackService.getTracksByReleaseYearAndArtist(year, artist), HttpStatus.OK);
            }
            return new ResponseEntity<>(trackService.getTracksByReleaseYear(year), HttpStatus.OK);
        } else if (artist!=null) {
            return new ResponseEntity<>(trackService.getTracksByArtist(artist), HttpStatus.OK);
        }
        return new ResponseEntity<>(trackService.getAllTracks(), HttpStatus.OK);
    }

    @GetMapping("/{id}")
//    @ResponseBody
    public ResponseEntity<Track> returnTrackById(@PathVariable("id")int id){
        return new ResponseEntity<>(trackService.getTrackById(id), HttpStatus.OK);
    }

    @PostMapping(consumes = "application/json")
//    @ResponseBody
    public ResponseEntity<Track> createNewTrack(@RequestBody Track track){
        trackService.createNewTrack(track);
        return new ResponseEntity<>(track, HttpStatus.CREATED);
    }


}
