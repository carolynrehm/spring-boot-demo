FROM java:8
COPY target/spring-boot-demo-1.0-SNAPSHOT.jar .
EXPOSE 8082
CMD java -jar spring-boot-demo-1.0-SNAPSHOT.jar
